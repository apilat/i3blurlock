# i3blurlock
> Native program which uses i3lock to lock your screen with a blurred screenshot

## Installation

### Requirements
* gcc
* make
* libX11
* i3lock (binary)

### Compilation
```
make
sudo make install
```

## Usage
```
Usage: i3blurlock [OPTIONS]

  -?, --help       print this help dialog
  -v, --verbose    increases verbosity
  -d, --dryrun     doesn't run i3lock
  -n, --nofork     runs i3lock in the foreground
```

## TODO Roadmap
* Look at data structure to optimize CPU cache
* (Possibly) add effects to image after blurring
