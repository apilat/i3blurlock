#include <X11/X.h>
#include <X11/Xutil.h>

#include <getopt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#define I3LOCK_BINARY ((const char*) "/usr/bin/i3lock")

// Default values
#define BLUR_KERNEL_SIZE 11
#define BLUR_STANDARD_DEVIATION 4

int take_screenshot(unsigned char **buf, unsigned int *width, unsigned int *height);
int blur_image_box(unsigned char *scratch, unsigned char *buf, unsigned int width, unsigned int height, unsigned int kernel_size);
int blur_image_gaussian(unsigned char *scratch, unsigned char *buf, unsigned int width, unsigned int height, float stdv);
void create_kernel(float *kernel, unsigned int size, float stdv);
int save_image(unsigned char *buf, unsigned int width, unsigned int height, int fd);

void usage(char *name) {
    fprintf(stderr, "Usage: %s [OPTIONS]\n"
            "  -h, --help       print this help dialog\n"
            "  -v, --verbose    increases verbosity\n"
            "  -d, --dryrun     doesn't run i3lock\n"
            "  -n, --nofork     runs i3lock in the foreground\n"
            "  -G, --gaussian [stdv] forces gaussian blur with the specified standard deviation (default 4)\n"
            "  -B, --boxblur [kernel] forces box blur with the specified kernel size (default 11)\n"
            ,name);
}

int main(int argc, char *argv[]) {
#define fail(s) { \
    perror((s)); \
    return -1; \
    }

    const char *opts = "hvdnG::B::";
    const struct option longopts[] = {
        {"help",     no_argument,       NULL, 'h'},
        {"verbose",  no_argument,       NULL, 'v'},
        {"dryrun",   no_argument,       NULL, 'd'},
        {"nofork",   no_argument,       NULL, 'n'},
        {"gaussian", optional_argument, NULL, 'G'},
        {"boxblur",  optional_argument, NULL, 'B'},
        {0,         0,           0,     0 }
    };
    int c = 'u';

    int parserr = 0;
    int verbose = 0, nofork = 0, dryrun = 0;
    int gaussian = 1;
    float stdv = BLUR_STANDARD_DEVIATION;
    int box = 0;
    unsigned int ksize = BLUR_KERNEL_SIZE;

    while((c = getopt_long(argc, argv, opts, longopts, NULL)) != -1) {
        switch(c) {
        case 'h':
            usage(argv[0]);
            return 0;
        case 'v':
            verbose = 1;
            break;
        case 'n':
            nofork = 1;
            break;
        case 'd':
            dryrun = 1;
            break;
        case 'G':
            gaussian = 1;
            box = 0;
            if (optarg)
                stdv = strtof(optarg, NULL);
            else if (optind < argc &&
                    argv[optind] && argv[optind][0] &&
                    argv[optind][0] != '-')
                stdv = strtof(argv[optind++], NULL);
            break;
        case 'B':
            gaussian = 0;
            box = 1;
            if (optarg)
                ksize = strtoul(optarg, NULL, 10);
            else if (optind < argc &&
                    argv[optind] && argv[optind][0] &&
                    argv[optind][0] != '-')
                ksize = strtoul(argv[optind++], NULL, 10);
            break;
        case '?':
            parserr = 1;
            break;
        }
    }

    if (parserr) {
        usage(argv[0]);
        return 1;
    }

    unsigned char *buf = NULL, *scratch;
    unsigned int width, height;
    char filename[] = "/tmp/blurlock.XXXXXX.raw";
    int fd;

    if (verbose)
        puts("Running in verbose mode");

    if (take_screenshot(&buf, &width, &height)) fail("Failed to take a screenshot");

    if (verbose)
        puts("Screenshot taken");

    scratch = malloc(width * height * 3);
    if (scratch == NULL) fail("Failed to allocate enough memory for blur");

    if (gaussian) {
        if (verbose)
            printf("Blurring with gaussian (stdv %f)\n", stdv);
        if (blur_image_gaussian(scratch, buf, width, height, stdv))
            fail("Failed to blur image");
    } else if (box) {
        if (verbose)
            printf("Blurring with box blur (kernel size %u)\n", ksize);
        if (blur_image_box(scratch, buf, width, height, ksize))
            fail("Failed to blur image");
    }

    free(scratch);

    if (verbose)
        puts("Screenshot blurred");

    fd = mkstemps(filename, 4);
    if (fd == -1) fail("Failed to create temporary file");
    if (verbose)
        printf("Using temporary file %s\n", filename);

    if (save_image(buf, width, height, fd)) fail("Failed to save image");

    if (verbose)
        puts("Image saved");

    free(buf);
    close(fd);

    if (dryrun) {
        remove(filename);
        if (verbose) {
            puts("Removing temporary file");
            puts("Dry run - not starting i3lock");
        }
        return 0;
    }

    if (fork() == 0) {
        sleep(1);
        remove(filename);
        if (verbose)
            puts("Removing temporary file");
    } else {
        if (verbose)
            puts("Running i3lock");
        char fmt[32];
        if (snprintf(fmt, sizeof(fmt), "--raw=%ux%u:rgb", width, height) >= (int)sizeof(fmt)) {
            fail("Raw format string longer than buffer");
        }
        if (nofork)
            execl(I3LOCK_BINARY, "i3lock", "-n", "-i", filename, fmt, NULL);
        else
            execl(I3LOCK_BINARY, "i3lock", "-i", filename, fmt, NULL);
    }

    return 0;
#undef fail
}

int take_screenshot(unsigned char **buf, unsigned int *width, unsigned int *height) {
    Display *display = XOpenDisplay(NULL);
    Window root = DefaultRootWindow(display);
    XWindowAttributes gwa;

    XGetWindowAttributes(display, root, &gwa);
    *width = gwa.width;
    *height = gwa.height;

    if (*buf == NULL)
        *buf = calloc(*width, *height * 3);
    else
        *buf = realloc(*buf, *width * *height * 3);
    if (*buf == NULL) return -1;

    XImage *image = XGetImage(display, root, 0, 0, *width, *height, AllPlanes, ZPixmap);

    for (unsigned int x = 0; x < *width; x++)
        for (unsigned int y = 0; y < *height; y++) {
            unsigned long pixel = XGetPixel(image, x, y);
            (*buf)[(x + *width * y) * 3] = (pixel & image->red_mask) >> 16;
            (*buf)[(x + *width * y) * 3 + 1] = (pixel & image->green_mask) >> 8;
            (*buf)[(x + *width * y) * 3 + 2] = (pixel & image->blue_mask) >> 0;
        }

    XDestroyImage(image);
    XCloseDisplay(display);

    return 0;
}

int blur_image_box(unsigned char *scratch, unsigned char *buf, unsigned int width, unsigned int height, unsigned int kernel_size) {
#define pixel(b, x, y) (b)[((x) + (width) * (y)) * 3 + (c)]
    long kernel;
    unsigned int i, j, c;
    int allocd = 0;

    if (scratch == NULL) {
        allocd = 1;
        scratch = malloc(width * height * 3);
        if (scratch == NULL)
            return -1;
    }

    for (c = 0; c < 3; c++) { // each color space
        for (i = 0; i < width; i++) { // vertical blur
            for (kernel = 0, j = 0; j < height + kernel_size / 2; j++) {
                if (j < height) // add new pixel
                    kernel += pixel(buf, i, j);
                if (j >= kernel_size) // remove old pixel
                    kernel -= pixel(buf, i, j - kernel_size);
                if (j >= kernel_size / 2) // write new value
                    pixel(scratch, i, j - kernel_size / 2) = kernel / kernel_size;
            }
        }
        for (j = 0; j < height; j++) { // horizontal blur
            for (kernel = 0, i = 0; i < width + kernel_size / 2; i++) {
                if (i < width)
                    kernel += pixel(scratch, i, j);
                if (i >= kernel_size)
                    kernel -= pixel(scratch, i - kernel_size, j);
                if (i >= kernel_size / 2)
                    pixel(buf, i - kernel_size / 2, j) = kernel / kernel_size;
            }
        }
    }

    if (allocd)
        free(scratch);

    return 0;
#undef pixel
}

int blur_image_gaussian(unsigned char *scratch, unsigned char *buf, unsigned int width, unsigned int height, float stdv) {
#define pixel(b, x, y) (b)[((x) + width * (y)) * 3 + color]
    unsigned int i, j, color, pos;
    signed int base;
    int allocd = 0;
    float *k, sum;
    unsigned int kernel_size;

    kernel_size = (int) floor(stdv * 3);
    if (kernel_size % 2 == 0)
        kernel_size++;
    k = malloc(kernel_size);
    if (k == NULL)
        return -1;

    create_kernel(k, kernel_size, stdv);

    if (scratch == NULL) {
        allocd = 1;
        scratch = malloc(width * height * 3);
        if (scratch == NULL)
            return -1;
    }

    for (color = 0; color < 3; color++) {
        for (i = 0; i < width; i++)
            for (j = 0; j < height; j++) {
                sum = 0;
                base = j - kernel_size/2;
                for (pos = 0; pos < kernel_size; pos++)
                    sum += (0 <= (signed)pos+base && base+pos < height) ? k[pos] * pixel(buf, i, base+pos) : 0;
                pixel(scratch, i, j) = (unsigned char)sum;
            }
        for (j = 0; j < height; j++) 
            for (i = 0; i < width ; i++) {
                sum = 0;
                base = i - kernel_size/2;
                for (pos = 0; pos < kernel_size; pos++)
                    sum += (0 <= (signed)pos+base && base+pos < width) ? k[pos] * pixel(scratch, base+pos, j) : 0;
                pixel(buf, i, j) = (unsigned char)sum;
            }
    }

    if (allocd)
        free(scratch);

    free(k);

    return 0;
#undef pixel
}

void create_kernel(float *kernel, unsigned int size, float stdv) {
    unsigned int x;
    float r2, s, sum;
    s = 2 * stdv * stdv;
    sum = 0;

    for (unsigned int i = 0; i < size; i++) {
        x = i - (size / 2);
        r2 = x * x;
        kernel[i] = expf(-r2 / s) / (M_PI * s);
        sum += kernel[i];
    }

    for (unsigned int i = 0; i < size; i++)
        kernel[i] /= sum;
}

int save_image(unsigned char *buf, unsigned int width, unsigned int height, int fd) {
    size_t written = 0;
    while (written < width * height * 3) {
        ssize_t ret = write(fd, buf + written, width * height * 3 - written);
        if (ret == -1) {
            return -1;
        }
        written += ret;
    }
    return 0;
}
