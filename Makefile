CC=gcc
CFLAGS=-Wall -Wextra
LIBS=-lX11 -lm

NAME=i3blurlock

all: $(NAME)
$(NAME): main.c
	gcc $(CFLAGS) -O2 $(LIBS) main.c -o $(NAME)

debug:
	$(CC) $(CFLAGS) -O0 -g $(LIBS) main.c -o $(NAME)

clean:
	rm -f $(NAME)

install:
	install -m755 $(NAME) /usr/local/bin

uninstall:
	rm -f /usr/local/bin/$(NAME)
